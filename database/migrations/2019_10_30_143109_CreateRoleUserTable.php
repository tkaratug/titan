<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreateRoleUserTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('role_user', function(CreateTable $table) {
            $table->integer('role_id')->unsigned();
            $table->integer('user_id')->size('big')->unsigned();

            $table->foreign('role_id')->references('roles', 'id')->onDelete('cascade');
            $table->foreign('user_id')->references('users', 'id')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('role_user');
    }
}