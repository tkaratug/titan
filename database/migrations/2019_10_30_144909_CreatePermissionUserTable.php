<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreatePermissionUserTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('permission_user', function(CreateTable $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('user_id')->size('big')->unsigned();

            $table->foreign('permission_id')->references('permissions', 'id')->onDelete('cascade');
            $table->foreign('user_id')->references('users', 'id')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('permission_user');
    }
}