<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreateFailedAttemptsTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('failed_attempts', function(CreateTable $table) {
            $table->integer('id')->size('big')->unsigned()->autoincrement()->primary();
            $table->string('email')->notNull();
            $table->string('ip')->notNull();
            $table->timestamp('created_at')->notNull();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('failed_attempts');
    }
}