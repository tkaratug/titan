<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('users', function(CreateTable $table) {
            $table->integer('id')->size('big')->unsigned()->autoincrement()->primary();
            $table->string('name')->notNull();
            $table->string('email')->notNull();
            $table->string('password')->notNull();
            $table->string('activation_code');
            $table->boolean('active')->defaultValue(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('users');
    }
}