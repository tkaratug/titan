<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreatePasswordResetTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('password_resets', function(CreateTable $table) {
            $table->integer('id')->size('big')->unsigned()->autoincrement()->primary();
            $table->string('email')->notNull();
            $table->string('token')->notNull();
            $table->integer('status')->size('tiny')->defaultValue(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('password_resets');
    }
}