<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use Opis\Database\Schema\CreateTable;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->db->schema()->create('permissions', function(CreateTable $table) {
            $table->integer('id')->unsigned()->autoincrement()->primary();
            $table->string('name')->notNull();
            $table->string('display_name')->notNull();
            $table->text('description');
            $table->timestamps();
            $table->unique('name');
        });
    }
    
    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->db->schema()->drop('permissions');
    }
}