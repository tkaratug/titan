<?php
/**
 * Titan Mini Framework
 * --
 * Simple and Modern Web Application Framework
 *
 * @author  Turan Karatuğ - <tkaratug@hotmail.com.tr>
 * @web     <http://www.titanphp.com>
 * @docs    <http://docs.titanphp.com>
 * @github  <http://github.com/tkaratug/titanframework>
 * @license The MIT License (MIT) - <http://opensource.org/licenses/MIT>
 */

/*
|--------------------------------------------------------------------------
| Require Core File
|--------------------------------------------------------------------------
|
| It includes the necessary things for the operation of the system.
|
*/
require_once __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Require Starter
|--------------------------------------------------------------------------
|
| It includes the starter file.
|
*/
require_once __DIR__ . '/../vendor/tkaratug/titan-core/src/Starter.php';