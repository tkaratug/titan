<?php

return [
    /*
    |---------------------------------------------------------------------
    | SMTP mail server
    |---------------------------------------------------------------------
    */
    'server'    => env('SMTP_MAIL_SERVER', ''),

    /*
    |---------------------------------------------------------------------
    | SMTP mail port
    |---------------------------------------------------------------------
    */
    'port'      => env('SMTP_MAIL_PORT', ''),

    /*
    |---------------------------------------------------------------------
    | SMTP mail account username
    |---------------------------------------------------------------------
    */
    'username'  => env('SMTP_MAIL_USERNAME', ''),

    /*
    |---------------------------------------------------------------------
    | SMTP mail account password
    |---------------------------------------------------------------------
    */
    'userpass'  => env('SMTP_MAIL_PASSWORD', ''),

    /*
    |---------------------------------------------------------------------
    | SMTP mail charset
    |---------------------------------------------------------------------
    */
    'charset'   => env('SMTP_MAIL_CHARSET', 'utf-8'),
];