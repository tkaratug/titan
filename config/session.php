<?php

return [
    /*
    |------------------------------------------------------------------------------
    | Encryption key for security
    |------------------------------------------------------------------------------
     */
    'encryption_key'    => env('APP_KEY', ''),

    /*
    |------------------------------------------------------------------------------
    | If true, cookie will only be sent over secure connections.
    |
    | It makes sure the browser only sends the session cookie in secure HTTPS
    | requests.
    |------------------------------------------------------------------------------
     */
    'cookie_secure'     => false,

    /*
    |------------------------------------------------------------------------------
    | If true, PHP will attempt to send the httponly flag when setting the session.
    |
    | It stops JavaScript from accessing the session cookie, preventing common
    | XSS attacks.
    |------------------------------------------------------------------------------
     */
    'cookie_httponly'   => true,

    /*
    |------------------------------------------------------------------------------
    | Specifies whether the module will only use cookies to store the session id
    | on the client side.
    |
    | Enabling this setting prevents attacks involved passing session ids in URLs.
    |------------------------------------------------------------------------------
    */
    'use_only_cookies'  => true,

    /*
    |------------------------------------------------------------------------------
    | Specifies the lifetime of the cookie in seconds which is sent to the browser.
    |------------------------------------------------------------------------------
    */
    'lifetime'          => 7200,
];
