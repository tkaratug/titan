<?php

return [
    /*
    |--------------------------------------------------------------------------
    | php titan app:key
    |--------------------------------------------------------------------------
    |
    | This command makes you to generate and set the application key.
    |
    */
    'KeyCommand' => Titan\Console\Commands\App\KeyCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan serve
    |--------------------------------------------------------------------------
    |
    | This command allows you to serve the application on your local server.
    |
    */
    'ServeCommand' => Titan\Console\Commands\App\ServeCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan clear:cache
    |--------------------------------------------------------------------------
    |
    | This command makes you to clear all cache files.
    |
    */
    'CacheCommand' => Titan\Console\Commands\Clear\CacheCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan clear:log
    |--------------------------------------------------------------------------
    |
    | This command makes you to clear all log files.
    |
    */
    'LogCommand' => Titan\Console\Commands\Clear\LogCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:controller
    |--------------------------------------------------------------------------
    |
    | This command makes you to create a controller
    |
    */
    'ControllerCommand' => Titan\Console\Commands\Make\ControllerCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:event
    |--------------------------------------------------------------------------
    |
    | This command makes you to create an event
    |
    */
    'EventCommand' => Titan\Console\Commands\Make\EventCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:listener
    |--------------------------------------------------------------------------
    |
    | This command makes you to create a listener
    |
    */
    'ListenerCommand' => Titan\Console\Commands\Make\ListenerCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:middleware
    |--------------------------------------------------------------------------
    |
    | This command makes you to create a middleware
    |
    */
    'MiddlewareCommand' => Titan\Console\Commands\Make\MiddlewareCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:migration
    |--------------------------------------------------------------------------
    |
    | This command makes you to create a migration
    |
    */
    'MigrationCommand' => Titan\Console\Commands\Make\MigrationCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:model
    |--------------------------------------------------------------------------
    |
    | This command makes you to create a model
    |
    */
    'ModelCommand' => Titan\Console\Commands\Make\ModelCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:seeder
    |--------------------------------------------------------------------------
    |
    | This command allows you to create new seeder.
    |
    */
    'SeederCommand' => Titan\Console\Commands\Make\SeederCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan make:service
    |--------------------------------------------------------------------------
    |
    | This command allows you to create new service.
    |
    */
    'ServiceCommand' => Titan\Console\Commands\Make\ServiceCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan db:migrate
    |--------------------------------------------------------------------------
    |
    | This command makes you to run all migrations.
    |
    */
    'MigrateCommand' => Titan\Console\Commands\Migrations\MigrateCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan db:rollback
    |--------------------------------------------------------------------------
    |
    | This command makes you to rollback the last batch of migrations
    |
    */
    'RollbackCommand' => Titan\Console\Commands\Migrations\RollbackCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan db:seed
    |--------------------------------------------------------------------------
    |
    | This command makes you to run all seeders.
    |
    */
    'SeedCommand' => Titan\Console\Commands\Seeders\SeedCommand::class,

    /*
    |--------------------------------------------------------------------------
    | php titan events:generate
    |--------------------------------------------------------------------------
    |
    | This command makes you to create events and listeners that have been set
    | in the service config file.
    |
    */
    'EventGenerateCommand' => Titan\Console\Commands\App\EventGenerateCommand::class,
];
