<?php

return [

    /*
    |--------------------------------------------------------------------------
    | The Registered Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application.
    |
    */
    'providers' => [

        /*
         * Titan Framework service providers.
         */
        Titan\Libraries\Router\RouterServiceProvider::class,
        Titan\Libraries\View\ViewServiceProvider::class,
        Titan\Libraries\Auth\AuthServiceProvider::class,
        Titan\Libraries\Hash\HashServiceProvider::class,
        Titan\Libraries\Log\LogServiceProvider::class,
        Titan\Libraries\Session\SessionServiceProvider::class,
        Titan\Libraries\Cookie\CookieServiceProvider::class,
        Titan\Libraries\Http\Request\RequestServiceProvider::class,
        Titan\Libraries\Http\Response\ResponseServiceProvider::class,
        Titan\Libraries\Http\Client\ClientServiceProvider::class,
        Titan\Libraries\Http\Jwt\JwtServiceProvider::class,
        Titan\Libraries\Event\EventServiceProvider::class,
        Titan\Libraries\Cache\CacheServiceProvider::class,
        Titan\Libraries\Database\ModelServiceProvider::class,
        Titan\Libraries\Database\DBServiceProvider::class,
        Titan\Libraries\Benchmark\BenchmarkServiceProvider::class,
        Titan\Libraries\Language\LanguageServiceProvider::class,
        Titan\Libraries\Date\DateServiceProvider::class,
        Titan\Libraries\Validation\ValidationServiceProvider::class,
        Titan\Libraries\Mail\MailServiceProvider::class,
        Titan\Libraries\Uri\UriServiceProvider::class,
        Titan\Libraries\Database\Migration\MigrationServiceProvider::class,
        Titan\Libraries\Database\Seeder\SeederServiceProvider::class,

        /*
         * Application service providers
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,

        /*
         * Third party service providers.
         */
    ],

    /*
    |--------------------------------------------------------------------------
    | Aliases for registered providers
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application is
    | started.
    |
    */
    'facades' => [
        'Load'      => Titan\Facades\Load::class,
        'Config'    => Titan\Facades\Config::class,
        'Route'     => Titan\Facades\Router::class,
        'View'      => Titan\Facades\View::class,
        'Auth'      => Titan\Facades\Auth::class,
        'Hash'      => Titan\Facades\Hash::class,
        'Log'       => Titan\Facades\Log::class,
        'Session'   => Titan\Facades\Session::class,
        'Cookie'    => Titan\Facades\Cookie::class,
        'Request'   => Titan\Facades\Request::class,
        'Response'  => Titan\Facades\Response::class,
        'Client'    => Titan\Facades\Client::class,
        'Jwt'       => Titan\Facades\Jwt::class,
        'Event'     => Titan\Facades\Event::class,
        'Cache'     => Titan\Facades\Cache::class,
        'Model'     => Titan\Facades\Model::class,
        'DB'        => Titan\Facades\DB::class,
        'Benchmark' => Titan\Facades\Benchmark::class,
        'Language'  => Titan\Facades\Language::class,
        'Date'      => Titan\Facades\Date::class,
        'Validation'=> Titan\Facades\Validation::class,
        'Mail'      => Titan\Facades\Mail::class,
        'Uri'       => Titan\Facades\Uri::class,

        /*
         * Aliases for registered third party providers
         */
    ],

    /*
    |--------------------------------------------------------------------------
    | The Application's Middleware stack
    |--------------------------------------------------------------------------
    |
    | These middleware are run during every request to your application.
    |
    */
    'middleware' => [
        'default'   => [
            'VerifyCsrfMiddleware'  => App\Middlewares\VerifyCsrfMiddleware::class,
        ],
        'manual'    => [
            'AuthMiddleware'        => App\Middlewares\AuthMiddleware::class,
            'GuestMiddleware'       => App\Middlewares\GuestMiddleware::class,
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Event & Listeners
    |--------------------------------------------------------------------------
    |
    | The event listener mappings for the application
    |
    */
    'events' => [
        'core' => [
            'Titan\Libraries\Auth\Events\ResetPasswordEvent' => [
                'Titan\Libraries\Auth\Listeners\ResetPasswordListener'
            ],
            'Titan\Libraries\Auth\Events\ActivationEvent' => [
                'Titan\Libraries\Auth\Listeners\ActivationListener'
            ],
        ],
        'app'  => [

        ],
    ],

];
