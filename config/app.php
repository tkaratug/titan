<?php

return [

    /*
    |---------------------------------------------------------------------
    | The default locale that will be used by the translation.
    |---------------------------------------------------------------------
    */
    'locale'        => env('LOCALE', 'tr'),

    /*
    |---------------------------------------------------------------------
    | Supported languages for the translation
    |---------------------------------------------------------------------
    */
    'languages'     => [
        'tr'                => ['code' => 'tr', 'info' => 'Turkish', 'name' => 'Türkçe', 'locale' => 'tr_TR'],
        'en'                => ['code' => 'en', 'info' => 'English', 'name' => 'English', 'locale' => 'en_EN']
    ],

    /*
    |---------------------------------------------------------------------
    | The default Timezone for your website.
    |---------------------------------------------------------------------
    */
    'timezone'      => env('TIMEZONE', 'Europe/Istanbul'),

    /*
    |---------------------------------------------------------------------
    | Application encryption key
    |---------------------------------------------------------------------
     */
    'key'           => env('APP_KEY', ''),

    /*
    |---------------------------------------------------------------------
    | Cookie configuration parameters
    |---------------------------------------------------------------------
    */
    'cookie'        => [
        'encryption_key'    => env('APP_KEY', ''),
        'cookie_security'   => true,
        'http_only'         => true,
        'secure'            => false,
        'separator'         => '--',
        'path'              => '/',
        'domain'            => '',
    ],

    /*
    |---------------------------------------------------------------------
    | Simple http cache configuration parameters
    |---------------------------------------------------------------------
    */
    'cache'         => [
        'path'              => 'storage' . DIRECTORY_SEPARATOR . 'cache',
        'extension'         => '.cache',
        'filename'          => 'default-cache',
        'expire'            => 604800,
    ],

];