<?php

// 404
Route::get('/404', function() {
    return view()->render('404');
})->name('404');

// Home
Route::get('/', 'HomeController@index');