<?php

namespace App\Controllers\Auth;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;
use Titan\Libraries\Validation\Validation;
use Titan\Facades\Auth;

class ResetPasswordController extends Controller
{
    /**
     * Password reset form
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->render('auth.reset_password', ['flash' => $this->getFlash()]);
    }

    /**
     * Password reset action
     *
     * @param Request $request
     * @param Validation $validation
     * @return mixed
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function reset(Request $request, Validation $validation)
    {
        // Validation rules
        $validation->rules([
            'email' => ['label' => translate('auth', 'email'), 'rules' => 'required|email']
        ], $request->post());

        // If the form is not valid return error
        if (!$validation->isValid()) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $validation->errorMessages()
            ], $this->getPath('resetForm'));
        }

        // Reset password
        try {
            Auth::resetPassword($request->post('email'));
            return $this->setFlash([
                'type'  => 'success',
                'text'  => translate('auth', 'reset_password_success')
            ], $this->getPath('resetForm'));
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('resetForm'));
        }
    }

    /**
     * Set new password form
     *
     * @param string $token
     * @return mixed
     */
    public function new(string $token)
    {
        // Find password reset data
        $data = Auth::getResetPasswordToken($token);

        // If data not found redirect to 404
        if (empty($data)) {
            return $this->redirectToRoute('404');
        }

        // View the password set form
        return $this->render('auth.set_password', ['token' => $token, 'flash' => $this->getFlash()]);
    }

    /**
     * Set new password
     *
     * @param Request $request
     * @param Validation $validation
     * @param string $token
     * @return mixed
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function update(Request $request, Validation $validation, string $token)
    {
        // Validation rules
        $validation->rules([
            'password'          => ['label' => translate('auth', 'password'), 'rules' => 'required|min_len:6'],
            'password_confirm'  => ['label' => translate('auth', 'password_confirm'), 'rules' => 'required|min_len:6|matches:password']
        ], $request->post());

        // If the form is not valid return error
        if (!$validation->isValid()) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $validation->errorMessages()
            ], $this->getPath('newPasswordForm', ['token' => $token]));
        }

        // Find password reset data
        $data = Auth::getResetPasswordToken($token);

        // If data not found redirect to 404
        if (empty($data)) {
            return $this->redirectToRoute('404');
        }

        // Set new password
        try {
            Auth::setNewPassword($request->post('password'), $data->email);
            return $this->setFlash([
                'type' => 'success',
                'text' => translate('auth', 'set_password_success')
            ], $this->getPath('loginForm'));
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('newPasswordForm', ['token' => $token]));
        }
    }
}