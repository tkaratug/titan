<?php

namespace App\Controllers\Auth;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;
use Titan\Libraries\Validation\Validation;
use Titan\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * Register form
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->render('auth.register', ['flash' => $this->getFlash()]);
    }

    /**
     * Register action
     *
     * @param Request $request
     * @param Validation $validation
     * @return mixed
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function register(Request $request, Validation $validation)
    {
        // Validation rules
        $validation->rules([
            'name'              => ['label' => translate('auth', 'name'), 'rules' => ['required']],
            'email'             => ['label' => translate('auth', 'email'), 'rules' => 'required|email'],
            'password'          => ['label' => translate('auth', 'password'), 'rules' => 'required|min_len:6'],
            'password_confirm'  => ['label' => translate('auth', 'password_confirm'), 'rules' => 'required|min_len:6|matches:password']
        ], $request->post());

        // If the form is not valid return error
        if (!$validation->isValid()) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $validation->errorMessages()
            ], $this->getPath('registerForm'));
        }

        // If email is exist return error
        if (Auth::isDuplicateEmail($request->post('email'))) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => '<p>' . translate('auth', 'duplicate_email_error') . '</p>'
            ], $this->getPath('registerForm'));
        }

        // Register
        try {
            Auth::register($request->post());

            // if activation is set as true, set flash message and redirect
            if (Auth::config('activation') === true) {
                return $this->setFlash([
                    'type' => 'success',
                    'text' => '<p>' . translate('auth', 'register_activation') . '</p>'
                ], $this->getPath('registerForm'));
            }

            return $this->setFlash([
                'type' => 'success',
                'text' => '<p>' . translate('auth', 'register_success') . '</p>'
            ], $this->getPath('loginForm'));
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('registerForm'));
        }
    }

    /**
     * Activation form
     *
     * @return mixed
     */
    public function activation()
    {
        return $this->render('auth.activation', ['flash' => $this->getFlash()]);
    }

    /**
     * Send activation code
     *
     * @param Request $request
     * @param Validation $validation
     * @return mixed
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function sendActivation(Request $request, Validation $validation)
    {
        // Validation rules
        $validation->rules([
            'email' => ['label' => translate('auth', 'email'), 'rules' => 'required|email']
        ], $request->post());

        // If the form is not valid return error
        if (!$validation->isValid()) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $validation->errorMessages()
            ], $this->getPath('activationForm'));
        }

        // Send
        try {
            Auth::resendActivation($request->post('email'));

            return $this->setFlash([
                'type'  => 'success',
                'text'  => translate('auth', 'send_activation_success')
            ], $this->getPath('activationForm'));
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('activationForm'));
        }
    }

    /**
     * Activate the user with given activation code
     *
     * @param $code
     * @return mixed
     */
    public function verify($code)
    {
        try {
            // Activate user
            Auth::activate($code);

            return $this->setFlash([
                'type' => 'success',
                'text' => '<p>' . translate('auth', 'activation_success') . '</p>'
            ], $this->getPath('loginForm'));
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('registerForm'));
        }
    }
}