<?php

namespace App\Controllers\Auth;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;
use Titan\Facades\Auth;

class LogoutController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        Auth::logout();
        $this->redirectToRoute('loginForm');
    }
}