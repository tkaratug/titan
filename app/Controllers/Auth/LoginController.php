<?php

namespace App\Controllers\Auth;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;
use Titan\Libraries\Validation\Validation;
use Titan\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Login form
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->render('auth.login', ['flash' => $this->getFlash()]);
    }

    /**
     * Login action
     *
     * @param Request $request
     * @param Validation $validation
     * @return mixed
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function login(Request $request, Validation $validation)
    {
        // Validation rules
        $validation->rules([
            'email' => ['label' => translate('auth', 'email'), 'rules' => 'required|email'],
            'password' => ['label' => translate('auth', 'password'), 'rules' => 'required|min_len:6']
        ], $request->post());

        // If the form is not valid return error
        if (!$validation->isValid()) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $validation->errorMessages()
            ], $this->getPath('loginForm'));
        }

        // Login
        try {
            Auth::login($request->post('email'), $request->post('password'));
            return $this->redirectToRoute('dashboard');
        } catch (\Exception $e) {
            return $this->setFlash([
                'type' => 'danger',
                'text' => $e->getMessage()
            ], $this->getPath('loginForm'));
        }
    }
}