<?php

namespace App\Controllers\Auth;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->render('auth.dashboard');
    }
}