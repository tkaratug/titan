<?php

namespace App\Controllers;

use Titan\Controller\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $this->render('home');
    }
}