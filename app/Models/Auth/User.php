<?php
namespace App\Models\Auth;

use Titan\Facades\DB;

class User
{
    /**
     * Is table exist
     *
     * @return mixed
     */
    public function isTableExist()
    {
        return DB::schema()->hasTable('users');
    }

    /**
     * Get record count by given email
     *
     * @param string $email
     * @return mixed
     */
    public function isDuplicateMail(string $email)
    {
        return DB::from('users')->where('email')->is($email)->count();
    }

    /**
     * Register
     *
     * @param array $data
     * @return bool
     */
    public function register(array $data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (DB::insert($data)->into('users')) {
            return $this->getUserByMail($data['email']);
        }

        return false;
    }

    /**
     * Get user by email
     *
     * @param string $email
     * @return mixed
     */
    public function getUserByMail(string $email)
    {
        return DB::from('users')->where('email')->is($email)->select()->first();
    }

    /**
     * Get user by id
     *
     * @param int $id
     * @return mixed
     */
    public function getUserById(int $id)
    {
        return DB::from('users')->where('id')->is($id)->select()->first();
    }

    /**
     * Save reset password token
     *
     * @param string $email
     * @param string $token
     * @return mixed
     */
    public function saveResetPasswordToken(string $email, string $token)
    {
        if ($this->hasActiveResetPasswordToken($email)) {
            return DB::update('password_resets')->where('email')->is($email)->set(['token' => $token]);
        }

        return DB::insert([
            'email'         => $email,
            'token'         => $token,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ])->into('password_resets');
    }

    /**
     * Get reset password data
     *
     * @param string $token
     * @return mixed
     */
    public function getResetPasswordToken(string $token)
    {
        return DB::from('password_resets')
                 ->where('token')->is($token)
                 ->where('status')->is(1)
                 ->select()->first();
    }

    /**
     * Set new password
     *
     * @param string $password
     * @param string $email
     * @return mixed
     */
    public function setNewPassword(string $password, string $email)
    {
        $update = DB::update('users')->where('email')->is($email)->set(['password' => $password]);

        if ($update) {
            DB::update('password_resets')->where('email')->is($email)->set(['status' => 0]);
        }

        return $update;
    }

    /**
     * Add failed attempt
     *
     * @param array $data
     * @return mixed
     */
    public function addFailedAttempt(array $data)
    {
        return DB::insert($data)->into('failed_attempts');
    }

    /**
     * Purge failed attempts older than x minutes
     *
     * @param string $from
     * @return mixed
     */
    public function purgeFailedAttempts(string $from)
    {
        return DB::from('failed_attempts')->where('created_at')->lt($from)->delete();
    }

    /**
     * Get total failed attempts from given date by email
     *
     * @param string $email
     * @param string $from
     * @return mixed
     */
    public function getTotalFailsByMailFrom(string $email, string $from)
    {
        return DB::from('failed_attempts')->where('email')->is($email)->where('created_at')->gt($from)->count();
    }

    /**
     * Get total failed attempts from given date by ip address
     * @param string $ip
     * @param string $from
     * @return mixed
     */
    public function getTotalFailsByIpFrom(string $ip, string $from)
    {
        return DB::from('failed_attempts')->where('ip')->is($ip)->where('created_at')->gt($from)->count();
    }

    /**
     * Get first failed attempts from given date by email
     *
     * @param string $email
     * @param string $from
     * @return mixed
     */
    public function getFirstAttemptByMailFrom(string $email, string $from)
    {
        return DB::from('failed_attempts')
                 ->where('email')
                 ->is($email)
                 ->where('created_at')->gt($from)
                 ->orderBy('created_at', 'asc')
                 ->select()
                 ->first();
    }

    /**
     * Get first failed attempts from given date by ip address
     *
     * @param string $ip
     * @param string $from
     * @return mixed
     */
    public function getFirstAttemptByIpFrom(string $ip, string $from)
    {
        return DB::from('failed_attempts')
                 ->where('ip')
                 ->is($ip)
                 ->where('created_at')->gt($from)
                 ->orderBy('created_at', 'asc')
                 ->select()
                 ->first();
    }

    /**
     * Get user by activation code
     *
     * @param string $code
     * @return mixed
     */
    public function getUserByActivationCode(string $code)
    {
        return DB::from('users')->where('activation_code')->is($code)->select()->first();
    }

    /**
     * Activate user
     *
     * @param int $id
     * @return mixed
     */
    public function activateUser(int $id)
    {
        return DB::update('users')->where('id')->is($id)->set(['activation_code' => null, 'active' => 1]);
    }

    /**
     * Get user roles
     *
     * @param int $id
     * @return mixed
     */
    public function getRoles(int $id)
    {
        return DB::from('role_user')
            ->join('roles', function($join) {
                $join->on('role_user.role_id', 'roles.id');
            })
            ->where('role_user.user_id')->is($id)
            ->select('roles.*')
            ->all();
    }

    /**
     * Attach role to the user
     *
     * @param int $userId
     * @param int $roleId
     * @return mixed
     */
    public function attachRole(int $userId, int $roleId)
    {
        return DB::insert(['role_id' => $roleId, 'user_id' => $userId])->into('role_user');
    }

    /**
     * Detach role from the user
     *
     * @param int $userId
     * @param int $roleId
     * @return mixed
     */
    public function detachRole(int $userId, int $roleId)
    {
        return DB::from('role_user')->where('role_id')->is($roleId)->where('user_id')->is($userId)->delete();
    }

    /**
     * Get user permissions
     *
     * @param int $id
     * @return mixed
     */
    public function getPermissions(int $id)
    {
        return DB::from('permission_user')
            ->join('permissions', function($join) {
                $join->on('permission_user.permission_id', 'permissions.id');
            })
            ->where('permission_user.user_id')->is($id)
            ->select('permissions.*')
            ->all();
    }

    /**
     * Attach permission to the user
     *
     * @param int $userId
     * @param int $permissionId
     * @return mixed
     */
    public function attachPermission(int $userId, int $permissionId)
    {
        return DB::insert(['permission_id' => $permissionId, 'user_id' => $userId])->into('permission_user');
    }

    /**
     * Detach permission from the user
     *
     * @param int $userId
     * @param int $permissionId
     * @return mixed
     */
    public function detachPermission(int $userId, int $permissionId)
    {
        return DB::from('permission_user')->where('permission_id')->is($permissionId)->where('user_id')->is($userId)->delete();
    }

    /**
     * Check if there is an active reset password token for given email
     *
     * @param string $email
     * @return bool
     */
    private function hasActiveResetPasswordToken(string $email)
    {
        $token = DB::from('password_resets')->where('email')->is($email)->where('status')->is(0)->select()->first();

        return !empty($token);
    }
}