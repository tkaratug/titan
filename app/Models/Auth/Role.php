<?php
namespace App\Models\Auth;

use Titan\Facades\Date;
use Titan\Facades\DB;

class Role
{
    /**
     * Get role by name
     *
     * @param string $name
     * @return mixed
     */
    public function getByName(string $name)
    {
        return DB::from('roles')->where('name')->is($name)->select()->first();
    }

    /**
     * Get role permissions
     *
     * @param int $id
     * @return mixed
     */
    public function getPermissions(int $id)
    {
        return DB::from('permission_role')
            ->join('permissions', function($join) {
                $join->on('permission_role.permission_id', 'permissions.id');
            })
            ->where('permission_role.role_id')->is($id)
            ->select('permissions.*')
            ->all();
    }

    /**
     * Attach permission to the role
     *
     * @param int $roleId
     * @param int $permissionId
     * @return mixed
     */
    public function attachPermission(int $roleId, int $permissionId)
    {
        return DB::insert(['permission_id' => $permissionId, 'role_id' => $roleId])->into('permission_role');
    }

    /**
     * Detach permission from the role
     *
     * @param int $roleId
     * @param int $permissionId
     * @return mixed
     */
    public function detachPermission(int $roleId, int $permissionId)
    {
        return DB::from('permission_role')->where('permission_id')->is($permissionId)->where('role_id')->is($roleId)->delete();
    }

    /**
     * Delete role
     *
     * @param \Titan\Libraries\Auth\Role $role
     * @return mixed
     */
    public function delete(\Titan\Libraries\Auth\Role $role)
    {
        return DB::from('roles')->where('id')->is($role->id)->delete();
    }

    /**
     * Save role data
     *
     * @param \Titan\Libraries\Auth\Role $role
     * @return mixed
     */
    public function save(\Titan\Libraries\Auth\Role $role)
    {
        if (empty($role->id)) {
            return $this->insert($role);
        }

        return $this->update($role);
    }

    /**
     * Insert new role
     *
     * @param \Titan\Libraries\Auth\Role $role
     * @return mixed
     */
    private function insert(\Titan\Libraries\Auth\Role $role)
    {
        return DB::insert([
            'name'          => $role->name,
            'display_name'  => $role->display_name,
            'description'   => $role->description,
            'created_at'    => Date::now(),
            'updated_at'    => Date::now()
        ])->into('roles');
    }

    /**
     * Update role
     *
     * @param \Titan\Libraries\Auth\Role $role
     * @return mixed
     */
    private function update(\Titan\Libraries\Auth\Role $role)
    {
        return DB::update('roles')
            ->where('id')->is($role->id)
            ->set([
                'name'          => $role->name,
                'display_name'  => $role->display_name,
                'description'   => $role->description,
                'updated_at'    => Date::now()
            ]);
    }
}