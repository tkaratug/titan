<?php
namespace App\Models\Auth;

use Titan\Facades\Date;
use Titan\Facades\DB;

class Permission
{
    /**
     * Get permission by name
     *
     * @param string $name
     * @return mixed
     */
    public function getByName(string $name)
    {
        return DB::from('permissions')->where('name')->is($name)->select()->first();
    }

    /**
     * Delete permission
     *
     * @param \Titan\Libraries\Auth\Permission $permission
     * @return mixed
     */
    public function delete(\Titan\Libraries\Auth\Permission $permission)
    {
        return DB::from('permissions')->where('id')->is($permission->id)->delete();
    }

    /**
     * Save permission data
     *
     * @param \Titan\Libraries\Auth\Permission $permission
     * @return mixed
     */
    public function save(\Titan\Libraries\Auth\Permission $permission)
    {
        if (empty($permission->id)) {
            return $this->insert($permission);
        }

        return $this->update($permission);
    }

    /**
     * Insert new permission
     *
     * @param \Titan\Libraries\Auth\Permission $permission
     * @return mixed
     */
    private function insert(\Titan\Libraries\Auth\Permission $permission)
    {
        return DB::insert([
            'name'          => $permission->name,
            'display_name'  => $permission->display_name,
            'description'   => $permission->description,
            'created_at'    => Date::now(),
            'updated_at'    => Date::now()
        ])->into('permissions');
    }

    /**
     * Update permission
     *
     * @param \Titan\Libraries\Auth\Permission $permission
     * @return mixed
     */
    private function update(\Titan\Libraries\Auth\Permission $permission)
    {
        return DB::update('permissions')
            ->where('id')->is($permission->id)
            ->set([
                'name'          => $permission->name,
                'display_name'  => $permission->display_name,
                'description'   => $permission->description,
                'updated_at'    => Date::now()
            ]);
    }
}