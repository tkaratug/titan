<?php

namespace App\Providers;

use Titan\Kernel\ServiceProvider;
use Titan\Facades\Router as Route;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap Auth service
     */
    public function boot()
    {
        // Auth
        Route::middleware(['GuestMiddleware'])->group(function() {
            Route::get('/login', 'Auth\LoginController@index')->name('loginForm');
            Route::post('/login', 'Auth\LoginController@login')->name('loginAction');
            Route::get('/register', 'Auth\RegisterController@index')->name('registerForm');
            Route::post('/register', 'Auth\RegisterController@register')->name('registerAction');
            Route::get('/verify/{code}', 'Auth\RegisterController@verify')->where(['code' => '(\w+)'])->name('verify');
            Route::get('/activation', 'Auth\RegisterController@activation')->name('activationForm');
            Route::post('/activation', 'Auth\RegisterController@sendActivation')->name('activationAction');
            Route::get('/resetPassword', 'Auth\ResetPasswordController@index')->name('resetForm');
            Route::post('/resetPassword', 'Auth\ResetPasswordController@reset')->name('resetAction');
            Route::get('/setPassword/{token}', 'Auth\ResetPasswordController@new')->where(['token' => '(\w+)'])->name('newPasswordForm');
            Route::post('/setPassword/{token}', 'Auth\ResetPasswordController@update')->where(['token' => '(\w+)'])->name('newPasswordAction');
        });

        // Dashboard
        Route::middleware(['AuthMiddleware'])->group(function() {
            Route::get('/dashboard', 'Auth\DashboardController@index')->name('dashboard');
            Route::post('/logout', 'Auth\LogoutController@index')->name('logout');
        });
    }
}