<?php

namespace App\Middlewares;

use Titan\Libraries\Session\Session;

class AuthMiddleware
{
    public function handle(Session $session)
    {
        if (!$session->has('user')) {
            return redirect('/login');
        }
    }
}