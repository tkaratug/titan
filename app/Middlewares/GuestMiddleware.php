<?php
namespace App\Middlewares;

use Titan\Libraries\Session\Session;

class GuestMiddleware
{
    /**
     * @param Session $session
     * @return mixed
     */
    public function handle(Session $session)
    {
        if ($session->has('user')) {
            return redirect('/dashboard');
        }
    }
}