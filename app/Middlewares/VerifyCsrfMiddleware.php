<?php

namespace App\Middlewares;

use Titan\Middleware\VerifyCsrf as Middleware;
use Titan\Libraries\Http\Request\Request;
use Titan\Facades\Router;

class VerifyCsrfMiddleware extends Middleware
{
    /**
     * These routes that should be excluded from CSRF verification
     *
     * @var array
     */
    protected $except = [];

    /**
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        return $this->verify($request, Router::currentRoute(), $this->except);
    }
}