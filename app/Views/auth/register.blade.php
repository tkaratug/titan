<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>{!! translate('auth', 'register') !!}</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext" rel="stylesheet">

    <!-- Authentication -->
    <style>
        body {
            font-family: Open Sans, sans-serif;
            font-size: 12px;
            background: #f2f2f2;
        }

        a {
            text-decoration: none;
            color: #bc5858;
        }

        a:hover {
            text-decoration: none;
            color: #3d79c1;
        }

        #header {
            width: 100%;
            height: 50px;
            background-color: #f8f8f8;
            border-bottom: 1px solid #ccc;
        }

        #header > #logo {
            width: 60%;
            margin: 0 auto;
        }

        #header > #logo > h2 {
            float: left;
            display: block;
            height: 100%;
            margin: 0;
            padding: 0;
            line-height: 50px;
            color: #bc5858;
            text-transform: uppercase;
            font-size: 24px;
        }

        #header > #nav {
            width: 60%;
            margin: 0 auto;
        }

        #header > #nav > ul {
            float: right;
            display: block;
            list-style: none;
            height: 100%;
            margin: 0;
            padding: 0;
            line-height: 50px;
        }

        #header > #nav > ul > li {
            float: left;
            margin-left: 10px;
            font-weight: bold;
        }

        .wrapper{
            margin-top: 45px;
        }
        .form-signin{
            max-width: 420px;
            padding: 30px 38px 25px;
            margin: 0 auto;
            border: 1px solid #cccccc;
            background: #fff;
        }
        .form-signin-heading { margin: 0 0 30px 0; }
        .input-group{
            height: 45px;
            margin-bottom: 15px;
            border-radius: 0px;
        }
        .form-control{
            height: 45px;
        }
        .btn-block{
            border-radius: 0px;
            margin-top: 25px;
            background-color: #bc5858;
            border: none;
            color: #fff;
        }
        .bol{
            position: relative;
            margin-top: -40px;
        }
        p.message { text-align: center; margin-top: 10px; font-size: 13px; }
    </style>

    <!-- Favicon -->
    <link rel="shortcut icon" sizes="16x16" href="{{ asset('images/favicon.png') }}" />
</head>
<body>
<div id="header">
    <div id="logo">
        <h2><a href="/">Titan MVC</a></h2>
    </div>
    <div id="nav">
        <ul>
            <li><a href="{!! link_to('login') !!}">{!! translate('auth', 'login') !!}</a></li>
            <li><a href="{!! link_to('register') !!}">{!! translate('auth', 'register') !!}</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="wrapper">
        <form action="/register" method="post" class="form-signin">
            <input type="hidden" name="csrf_token" value="{!! csrf_token() !!}">
            <h3 class="form-signin-heading text-center"><a href="/"><img src="{{ asset('images/titan.png') }}" width="90"></a></h3>
            <h4 class="text-center">{!! translate('auth', 'register') !!}</h4>
            <hr class="spartan">
            @if (!empty($flash))
                <div class="alert alert-{{ $flash['type'] }}">{!! $flash['text'] !!}</div>
                <hr class="spartan">
            @endif
            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-user"></span></span>
                <input type="text" class="form-control" name="name" placeholder="{!! translate('auth', 'name') !!}" required="" autofocus="">
            </div>
            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-envelope"></span></span>
                <input type="email" class="form-control" name="email" placeholder="{!! translate('auth', 'email') !!}" required="" autofocus="">
            </div>
            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" class="form-control" name="password" placeholder="{!! translate('auth', 'password') !!}" required="">
            </div>
            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" class="form-control" name="password_confirm" placeholder="{!! translate('auth', 'password_confirm') !!}" required="">
            </div>
            <button class="btn btn-info btn-block" type="Submit">{!! translate('auth', 'submit') !!}</button>
            <p class="message">{!! translate('auth', 'already_registered') !!} <a href="/login">{!! translate('auth', 'login') !!}</a></p>
        </form>
    </div>
</div>

<!-- Jquery -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>