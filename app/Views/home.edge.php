<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Titan MVC Framework v3</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:200" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" sizes="16x16" href="{{ asset('images/favicon.png') }}" />

    <style>
        * {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            padding: 0;
            margin: 0;
        }

        #wrapper {
            position: relative;
            height: 100vh;
        }

        #wrapper .container {
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .container {
            max-width: 767px;
            width: 100%;
            line-height: 1.4;
            text-align: center;
            padding: 15px;
        }

        .container .logo {
            position: relative;
            height: 220px;
        }

        .container .logo h1 {
            font-family: 'Kanit', sans-serif;
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            font-size: 186px;
            font-weight: 200;
            margin: 0px;
            background: linear-gradient(130deg, #ffa34f, #ff6f68);
            color:transparent;
            -webkit-background-clip: text;
            background-clip: text;
            text-transform: uppercase;
        }

        .container h2 {
            font-family: 'Kanit', sans-serif;
            font-size: 26px;
            font-weight: 600;
            text-transform: uppercase;
            margin-top: 0px;
            margin-bottom: 25px;
            letter-spacing: 3px;
            color: #666;
        }


        .container p {
            font-family: 'Kanit', sans-serif;
            font-size: 16px;
            font-weight: 200;
            margin-top: 0px;
            margin-bottom: 25px;
        }


        .container a {
            font-family: 'Kanit', sans-serif;
            color: #ff6f68;
            font-weight: 200;
            text-decoration: none;
            border-bottom: 1px dashed #ff6f68;
            border-radius: 2px;
        }

        @media only screen and (max-width: 480px) {
            .container .logo {
                position: relative;
                height: 168px;
            }

            .container .logo h1 {
                font-size: 142px;
            }

            .container h2 {
                font-size: 22px;
            }
        }
    </style>
</head>

<body>

<div id="wrapper">
    <div class="container">
        <div class="logo">
            <img src="{{ asset('images/titan.png') }}" />
        </div>
        <h2>SIMPLE MVC FRAMEWORK</h2>
        <p><a href="#">View on Github</a> | <a href="#">Documentation</a></p>
        <p>Version: <a href="#">3.0.0</a> | Created by <a href="#">Turan Karatug</a></p>
    </div>
</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
